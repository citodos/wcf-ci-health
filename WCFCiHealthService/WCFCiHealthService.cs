﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.ServiceModel;
using System.Threading.Tasks;

namespace WCFCiHealthService
{
    public partial class WCFCiHealthService : ServiceBase
    {
        List<ServiceHost> Services= new List<ServiceHost>();
        public WCFCiHealthService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            Services.Add(new ServiceHost(typeof(WcfCiHealth.ISEmpresa)));
            foreach (ServiceHost s in Services){
                 if(s.State == CommunicationState.Opened){
                    s.Close();
                 }            
            s.Open();
            }
           
      
        }

        protected override void OnStop()
        {
            foreach (ServiceHost s in Services){
                 if(s.State == CommunicationState.Opened){
                    s.Close();
                 }            
            }
                  
        }
    }
}
