﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfCiHealth;

namespace WcfCiHealth
{
   public class SEntidadeFisica : ISEntidadeFisica
   {
       CIHealthEntities1 context = new CIHealthEntities1();

       //Cadastro Entidade Fisica
       public Int32 GetCadastroEntFisica(Int32 identidade,
                                            Int32 idestcivil,
                                            Int32 idcor,
                                            Int32 cpf,
                                            DateTime dtnascimento,                                            
                                            String sexo,
                                            String pai,
                                            String mae,
                                            String naturalidade,
                                            String rg,
                                            String cns,
                                            String profissao)
       {
           Entidade_Fisica entfis = new Entidade_Fisica();
           try
           {
               entfis.identidade = identidade;
               entfis.idestcivil = idestcivil;
               entfis.idcor = idcor;
               entfis.cpf = cpf;
               entfis.dtnascimento = dtnascimento;
               entfis.sexo = sexo;
               entfis.pai = pai;
               entfis.mae = mae;
               entfis.naturalidade = naturalidade;
               entfis.rg = rg;
               entfis.cns = cns;
               entfis.profissao = profissao;
               context.Entidade_Fisica.Add(entfis);
               context.SaveChanges();

               return identidade;
           }
           catch (Exception e)
           {
               return 0;
           }
       }

       //Pesquisa Entidades Fisica
       public List<Entidade_Fisica> GetPesquisaEntFisica(Int32 identidade)
       {
           if (identidade != 0 && identidade != null)
           {
               List<Entidade_Fisica> entfisic = new List<Entidade_Fisica>();
               Entidade_Fisica entfis = new Entidade_Fisica();

               foreach (var result in context.Entidade_Fisica.Where(d => d.identidade == identidade))
               {
                   entfis.identidade = result.identidade;
                   entfis.idestcivil = result.idestcivil;
                   entfis.idcor = result.idcor;
                   entfis.cpf = result.cpf;
                   entfis.dtnascimento = result.dtnascimento;                   
                   entfis.sexo = result.sexo;
                   entfis.pai = result.pai;
                   entfis.mae = result.mae;
                   entfis.naturalidade = result.naturalidade;
                   entfis.rg = result.rg;
                   entfis.cns = result.cns;
                   entfis.profissao = result.profissao;
                   entfisic.Add(entfis);
               }
               return entfisic;
           }
           else
           {
               return null;
           }
       }

       //Exclusao Entidade Fisica
       public String GetExcluirEntFisica(Int32 identidade)
       {
           try
           {
               //var DeletarEntfisica = context.Entidade_Fisica.FirstOrDefault(p => p.identidade == identidade);
               //context.Entidade_Fisica.Remove(DeletarEntFisica);
               //context.SaveChanges();

               return "Exclusão realizada com Sucesso";
           }
           catch (Exception e)
           {
               return "Não Foi Possivel Realizar a Exclusão";
           }
       }

       //Alterar Entidade Fisica
       public String GetAlterarEntFisica(Int32 identidade,
                                            Int32 idestcivil,
                                            Int32 idcor,
                                            Int32 cpf,
                                            DateTime dtnascimento,                                            
                                            String sexo,
                                            String pai,
                                            String mae,
                                            String naturalidade,
                                            String rg,
                                            String cns,
                                            String profissao)
       {
           try
           {
               var atualizaEntFisica = context.Entidade_Fisica.FirstOrDefault(p => p.identidade == identidade);
               if (atualizaEntFisica != null)
               {
                   atualizaEntFisica.identidade = identidade;
                   atualizaEntFisica.idestcivil = idestcivil;
                   atualizaEntFisica.idcor = idcor;
                   atualizaEntFisica.cpf = cpf;
                   atualizaEntFisica.dtnascimento = dtnascimento;                   
                   atualizaEntFisica.sexo = sexo;
                   atualizaEntFisica.pai = pai;
                   atualizaEntFisica.mae = mae;
                   atualizaEntFisica.naturalidade = naturalidade;
                   atualizaEntFisica.rg = rg;
                   atualizaEntFisica.cns = cns;
                   atualizaEntFisica.profissao = profissao;
                   context.SaveChanges();

                   return "Alteração realizada com Sucesso";
               }
               else
               {
                   return "Este Local não consta em nossos registros";
               }
           }
           catch (Exception e)
           {
               return "Não Foi Possivel Realizar a Alteração";
           }
       }
    }
}
