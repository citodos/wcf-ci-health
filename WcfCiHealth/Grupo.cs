//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WcfCiHealth
{
    using System;
    using System.Collections.Generic;
    
    public partial class Grupo
    {
        public Grupo()
        {
            this.Menus = new HashSet<Menu>();
        }
    
        public int idgrupo { get; set; }
        public int idsistema { get; set; }
        public string dsgrupo { get; set; }
    
        public virtual Sistema Sistema { get; set; }
        public virtual ICollection<Menu> Menus { get; set; }
    }
}
