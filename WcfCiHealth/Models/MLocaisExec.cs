﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WcfCiHealth.Models
{
     [DataContract]
    public class MLocaisExec
    {
       [DataMember]
       public Nullable<int> idlocal{ get; set; }
       [DataMember]
       public Nullable<long> identidade { get; set; }
       [DataMember]
       public Nullable<long> identidade_operadora { get; set; }
       [DataMember]
       public string codoperadora { get; set; }

    
      }
}
