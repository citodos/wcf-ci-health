﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfCiHealth.Models
{
     [DataContract]
     public class MEntidade
    {
        [DataMember]
         public long identidade { get; set; }
        [DataMember]
        public string idtipo { get; set; }
        [DataMember]
        public string nome { get; set; }
        [DataMember]
        public string fone { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string celular { get; set; }
        [DataMember]
        public string flg_status { get; set; }
        [DataMember]
        public int identidade_vinculo { get; set; }
        [DataMember]
        public string nomecontato { get; set; }
        
      

    }
}
