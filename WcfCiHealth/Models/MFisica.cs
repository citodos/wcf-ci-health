﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfCiHealth.Models
{
    public class MFisica
    {
        [DataMember]
        public int idestcivil { get; set; }
        [DataMember]
        public int idcor { get; set; }
        [DataMember]
        public int cpf { get; set; }
        [DataMember]
        public System.DateTime dtnascimento { get; set; }
        [DataMember]
        public string sexo { get; set; }
        [DataMember]
        public string pai { get; set; }
        [DataMember]
        public string mae { get;  set;}
        [DataMember]
        public string naturalidade { get; set; }
        [DataMember]
        public string rg { get; set; }
        [DataMember]
        public string cns { get; set; }
        [DataMember]
        public string profissao { get; set; }
    }
}
