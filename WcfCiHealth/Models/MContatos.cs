﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WcfCiHealth.Models
{
          [DataContract]
        public class MContato
        {
            [DataMember]
            public int idcontato { get; set; }
            [DataMember]
            public int idcontatotipo { get; set; }
            [DataMember]
            public string dscontato { get; set; }
            [DataMember]
            public string nomecontato { get; set; }
        }
    }

