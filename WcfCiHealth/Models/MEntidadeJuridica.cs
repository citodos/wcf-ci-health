﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WcfCiHealth.Models
{
     [DataContract]
     public class MEntidadeJuridica
    {
       
         [DataMember]
         public string nomefantasia { get; set; }
         [DataMember]
         public string cnpj { get; set; }
         [DataMember]
         public string inscr_estadual { get; set; }
         [DataMember]
         public string inscr_municipal { get; set; }
         [DataMember]
         public string flg_hub { get; set; }
         [DataMember]
         public string cnes { get; set; }
         [DataMember]
         public string logo { get; set; }
         [DataMember]
         public Nullable<System.DateTime> dtultvalidacaonet { get; set; }
         [DataMember]
         public string codigoibge { get; set; }
         [DataMember]
         public Nullable<int> contrato { get; set; }
         [DataMember]
         public Nullable<int> limitediasagenda { get; set; }
         [DataMember]
         public Nullable<int> timerparaagendamento { get; set; }
         [DataMember]
         public Nullable<int> identidade_matriz { get; set; }
    }
}
