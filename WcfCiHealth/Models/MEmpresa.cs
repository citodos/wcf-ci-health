﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfCiHealth.Models
{
    [DataContract]
    public class MEmpresa
    {
        [DataMember]
        public int idorgunidade { get; set; }
        [DataMember]
        public string dsorg { get; set; }
        [DataMember]
        public string nome_fantasia { get; set; }
        [DataMember]
        public string cnpj { get; set; }
        [DataMember]
        public string inscr_estadual { get; set; }
        [DataMember]
        public string inscr_municipal { get; set; }
        [DataMember]
        public string flg_status { get; set; }
        [DataMember]
        public string flg_hub { get; set; }
        [DataMember]
        public Nullable<System.DateTime> dtexclusao { get; set; }
        [DataMember]
        public string cnes { get; set; }
        [DataMember]
        public string logo { get; set; }
        [DataMember]
        public Nullable<System.DateTime> dtultvalidacaonet { get; set; }
        [DataMember]
        public string codigoibge { get; set; }
        [DataMember]
        public Nullable<int> idorg { get; set; }
        [DataMember]
        public Nullable<int> contrato { get; set; }
        [DataMember]
        public List<MEndereco> endereco { get; set; }
        [DataMember]
        public List<MContato> contatos { get; set; }
        [DataMember]
        public List<MLocaisExec> localexe { get; set; }
        
    }
}
