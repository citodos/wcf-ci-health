﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WcfCiHealth.Models
{
    [DataContract]
    public class MEndereco
    {
        [DataMember]
        public int idendereco { get; set; }
        [DataMember]
        public string dsendereco { get; set; }
        [DataMember]
        public string nrendereco { get; set; }
        [DataMember]
        public string bairro { get; set; }
        [DataMember]
        public string cidade { get; set; }
        [DataMember]
        public string uf { get; set; }
        [DataMember]
        public Nullable<int> cep { get; set; }
        [DataMember]
        public string complemento { get; set; }
        [DataMember]
        public Nullable<int> idlogradouro { get; set; }
        [DataMember]
        public Nullable<int> idtipoendereco { get; set; }
    }
}
