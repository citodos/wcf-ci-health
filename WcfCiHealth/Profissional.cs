//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WcfCiHealth
{
    using System;
    using System.Collections.Generic;
    
    public partial class Profissional
    {
        public Profissional()
        {
            this.ContatoMovs = new HashSet<ContatoMov>();
            this.EnderecoMovs = new HashSet<EnderecoMov>();
        }
    
        public int idprofequip { get; set; }
        public string nome { get; set; }
        public string cfp { get; set; }
        public Nullable<decimal> crm { get; set; }
        public string cnpj { get; set; }
        public string ccm { get; set; }
        public string rg { get; set; }
        public Nullable<System.DateTime> dtcadastro { get; set; }
        public string responsavel { get; set; }
        public Nullable<System.DateTime> dtinicio { get; set; }
        public Nullable<System.DateTime> dtfim { get; set; }
        public string flg_profeqp { get; set; }
        public string flg_conselho { get; set; }
        public string flg_gerarAgenda { get; set; }
        public string flg_status { get; set; }
        public Nullable<int> flg_Idprofequip { get; set; }
        public string flg_profequip { get; set; }
        public int idorgunidade { get; set; }
    
        public virtual Empresa Empresa { get; set; }
        public virtual ICollection<ContatoMov> ContatoMovs { get; set; }
        public virtual ICollection<EnderecoMov> EnderecoMovs { get; set; }
    }
}
