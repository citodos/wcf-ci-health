﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using WcfCiHealth.Models;


namespace WcfCiHealth
{


    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISEmpresa" in both code and config file together.
    [ServiceContract]
    public interface ISEmpresa
    {   //PesquisaEmpresaPorID
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "PesquisaEmpresaId?idorgunidade={idorgunidade}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MEmpresa> GetPesquisarEmpresa(Int32 idorgunidade);

        //PesquisaEmpresaNome
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "PesquisaEmpresaNome", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<MEmpresa> GetPesquisarEmpresaNome(String dsorg = "", String nomefatasia = "", String cnpj = "");

        //CadastroEmpresa
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Cadastrar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Int32 GetCadastraEmpresa(String dsorg,
                                         String nome_fantasia = "",
                                         String cnpj = "",
                                         String inscr_estadual = "",
                                         String inscr_municipal = "",
                                         String flg_status = "",
                                         String flg_Hub = "",
                                         String cnes = "",
                                         String logo = "",
                                         String codigoibge = "",
                                         Int32 contrato = 0,
                                         Int32 id_unidade = 0);
        //AlterarEmpresa
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Alterar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetAlterarEmpresa(Int32 idorgunidade,
                                        String dsorg,
                                        String nome_fantasia = "",
                                        String cnpj = "",
                                        String inscr_estadual = "",
                                        String inscr_municipal = "",
                                        String flg_status = "",
                                        String flg_Hub = "",
                                        String cnes = "",
                                        String logo = "",
                                        String codigoibge = "",
                                        Int32 contrato = 0,
                                        Int32 idorg = 0);
        //ExcluirEmpresa
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Deletar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetExcluirEmpresa(Int32 idorgunidade);

       
    }
}
