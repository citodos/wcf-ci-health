﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfCiHealth
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISContatos" in both code and config file together.
    [ServiceContract]
    public interface ISContatos
    {
        //CadastroContato
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Cadastrar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Int32 GetCadastroContato(Int32 idcontatotipo,
                                           Int32 idorgunidade,
                                           String dscontato = "",
                                           String nomecontato = "");

        //PesquisaContato
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Pesquisa", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Contato> GetPesquisaContato(Int32 idcontato);

        //DeletarContato
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Deletar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetExcluirContato(Int32 idcontato);

        //AlterarContato
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Alterar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetAlterarContato(Int32 idcontato,
                                           Int32 idorgunidade,
                                           Int32 idcontatotipo,
                                           String dscontato = "",
                                           String nomecontato = "");
    }
}
