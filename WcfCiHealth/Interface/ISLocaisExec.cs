﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfCiHealth
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISLocaisExec" in both code and config file together.
    [ServiceContract]
    public interface ISLocaisExec
    {   
        //Cadastro Locais
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Cadastrar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Int32 GetCadastroLocais(Int32 idorgunidade,
                                        Int32 idconvenio,
                                        DateTime dtcadastro,
                                        Int32 idendereco,
                                        String dslocal = "",
                                        String cdoperadora = "",
                                        String cnes = "",
                                        String tipolocal = "",
                                        String flg_status = "");
                                        

        //Pesquisa Locais
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Pesquisar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<LocalExecSolic> GetPesquisaLocais(Int32 idlocal);
                                        

        //Excluir Locais
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Excluir", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetExcluirLocais(Int32 idlocal);
                                
        //Alterar Locais
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Alterar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetAlterarLocais(Int32 idlocal,
                                      Int32 Idorgunidade,
                                      Int32 Idconvenio,
                                      DateTime dtcadastro,
                                      String dslocal = "",
                                      String cdoperadora = "",
                                      String cnes = "",
                                      String tipolocal = "",
                                      String flg_status = "");
                                        
        
    }
}
