﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfCiHealth
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISContatos" in both code and config file together.
    [ServiceContract]
    
    public interface ISEntidadeFisica
    {
        //Cadastro Entidade Fisica
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Cadastrar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Int32 GetCadastroEntFisica(Int32 identidade,
                                        Int32 idestcivil,
                                        Int32 idcor,
                                        Int32 cpf,
                                        DateTime dtnascimento,                                        
                                        String sexo,
                                        String pai,
                                        String mae,
                                        String naturalidade,
                                        String rg,
                                        String cns,
                                        String profissao);

        //Pesquisa Entidade Fisica
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Pesquisa", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Entidade_Fisica> GetPesquisaEntFisica(Int32 identidade);

        //Excluir Entidade Fisica
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Deletar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetExcluirEntFisica(Int32 identidade);

        //Alterar Entidade Fisica
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Alterar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetAlterarEntFisica(Int32 identidade,
                                        Int32 idestcivil,
                                        Int32 idcor,
                                        Int32 cpf,
                                        DateTime dtnascimento,                                        
                                        String sexo,
                                        String pai,
                                        String mae,
                                        String naturalidade,
                                        String rg,
                                        String cns,
                                        String profissao);

    }
}
