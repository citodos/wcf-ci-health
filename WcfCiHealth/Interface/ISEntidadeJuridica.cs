﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;

namespace WcfCiHealth
{
   [ServiceContract]
   public interface ISEntidadeJuridica
    {
       //Cadastro Entidade Juridica
       [OperationContract]
       [WebInvoke(Method = "POST", UriTemplate = "Cadastrar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
       Int32 GetCadastroEntJuridica(Int32 identidade,
                                            Int32 contrato,
                                            Int32 limitediasagenda,
                                            Int32 timerparaagendamento,
                                            Int32 identidade_Matriz,
                                            DateTime dtultvalidacaonet,                                            
                                            String nomefantasia,
                                            String cnpj,
                                            String inscr_estadual,
                                            String inscr_municipal,
                                            String flg_hub,
                                            String cnes,
                                            String logo,
                                            String codigoibge);

       //Pesquisa Entidade Juridica
       [OperationContract]
       [WebInvoke(Method = "POST", UriTemplate = "Pesquisa", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
       List<Entidade_Juridica> GetPesquisaEntJuridica(Int32 identidade);

       //Excluir Entidade Juridica
       [OperationContract]
       [WebInvoke(Method = "POST", UriTemplate = "Deletar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
       String GetExcluirEntJuridica(Int32 identidade);

       //Alterar Entidade Juridica
       [OperationContract]
       [WebInvoke(Method = "POST", UriTemplate = "Alterar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
       String GetAlterarEntJuridica(Int32 identidade,
                                            Int32 contrato,
                                            Int32 limitediasagenda,
                                            Int32 timerparaagendamento,
                                            Int32 identidade_Matriz,
                                            DateTime dtultvalidacaonet,                                            
                                            String nomefantasia,
                                            String cnpj,
                                            String inscr_estadual,
                                            String inscr_municipal,
                                            String flg_hub,
                                            String cnes,
                                            String logo,
                                            String codigoibge);                                        
    }
}
