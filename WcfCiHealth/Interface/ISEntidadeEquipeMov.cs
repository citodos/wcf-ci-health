﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfCiHealth
{
    [ServiceContract]
    public interface ISEntidadeEquipeMov
    {
        //Cadastro Entidade Equipemov
        [WebInvoke(Method = "POST", UriTemplate = "Cadastrar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Int32 GetCadastroEntEquipe(Int32 idequipemov,
                                        Int32 identidade_equipe,
                                        Int32 identidade_profissional);

        //Pesquisa Entidade Equipemov
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Pesquisa", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Entidade_EquipeMov> GetPesquisaEntEquipe(Int32 idequipemov);

        //Exclusão Entidade Equipemov
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Deletar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetExcluirEntEquipe(Int32 idlocal);

        //Alterar Entidade Equipemov
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Alterar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetAlterarEntEquipe(Int32 idequipemov,
                                        Int32 identidade_equipe,
                                        Int32 identidade_profissional);    
    }
}
