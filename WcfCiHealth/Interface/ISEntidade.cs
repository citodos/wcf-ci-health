﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfCiHealth.Models;

namespace WcfCiHealth
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISContatos" in both code and config file together.
    [ServiceContract]
    public interface ISEntidade
    
    {
        

        //CadastroEntidadeJuridica
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CadastrarEntidadeJuridica", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long GetCadastroEntidadeJuridica(MEntidade entidade, MEntidadeJuridica entidadejuridica, MLocaisExec locaisExec = null, MEndereco endereco = null);

        //Cadastro Entidade Fisica
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CadastrarEntidadeFisica", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long GetCadastroEntidadeFisica(MEntidade entidade, Entidade_Fisica entidadefisica);
        
        //PesquisaEntidadeJuridica
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "PesquisaJuridica?identidade={identidade}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List <Entidade> GetPesquisaEntidadeJuridica(Int32 identidade);

        //PesquisaEntidadeFisica
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "PesquisaFisica", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Entidade GetPesquisaEntidadeFisica(Int32 identidade,
                                                        String idtipo);

        //DeletarEntidade
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Deletar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetExcluirEntidade(Int32 identidade);

        //AlterarEntidade
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Alterar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetAlterarEntidade(MEntidade entidade, MEntidadeJuridica entidadejuridica, MLocaisExec locaisExec = null, MEndereco endereco = null);

        //Pesquisa Convenios
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "PesquisaConvenio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Entidade> GetPesquisarConvenio();

        //Pesquisa Entidade Juridica via Colunas e conteudo 
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "PesquisaEntidadeJuriCampos?Coluna={Coluna}&Conteudo={Conteudo}&idTipo={idTipo}&idvinculo={idvinculo}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Entidade> GetPesquisaEntidadeJuriCampos(String Coluna, String Conteudo, String idTipo, int idvinculo = 0);
    }
}
