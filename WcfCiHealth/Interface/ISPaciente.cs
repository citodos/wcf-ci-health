﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfCiHealth.Interface
{
    [ServiceContract]
    public interface ISPaciente
    {//Cadastro Pacientes
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Cadastrar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Int32 GetCadastroPaciente(Int32 idpaciente,
                                        Int32 idrofissao,
                                        Int32 idestcivil, 
                                        Int32 idcor,
                                        Int32 cpf,
                                        Int32 idorg,
                                        DateTime dtcadastro,
                                        DateTime dtnascimento,
                                        DateTime dtexclusao,
                                        String paciente,
                                        String sexo,
                                        String pai,
                                        String mae, 
                                        String naturalidade,
                                        String rg,
                                        String cns);
         //Pesquisa Pacientes
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Pesquisar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Paciente> GetPesquisaPaciente(Int32 idpaciente);

        //Excluir Pacientes
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Excluir", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetExcluirPaciente(Int32 idPaciente);
      
        //Alterar Pacientes
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Alterar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetAlterarPaciente(Int32 idpaciente,
                                        Int32 idrofissao,
                                        Int32 idestcivil, 
                                        Int32 idcor,
                                        Int32 cpf,
                                        Int32 idorg,
                                        DateTime dtcadastro,
                                        DateTime dtnascimento,
                                        DateTime dtexclusao,
                                        String paciente,
                                        String sexo,
                                        String pai,
                                        String mae, 
                                        String naturalidade,
                                        String rg,
                                        String cns);
    }
}
