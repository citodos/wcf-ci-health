﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using WcfCiHealth.Models;

namespace WcfCiHealth
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISEndereco" in both code and config file together.
    [ServiceContract]
    public interface ISEndereco
    {
        //Pesquisa Endereço   
       
      //  [WebInvoke(Method = "POST", UriTemplate = "Pesquisa", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        [OperationContract]  
        [WebInvoke(Method = "GET", UriTemplate = "Pesquisa?idendereco={idendereco}&dsendereco={dsendereco}&bairro={bairro}&cidade={cidade}&uf={uf}&cep={cep}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Endereco> GetPesquisarEndereco(Int32 idendereco,
                                                    String dsendereco = "",
                                                    String bairro = "",
                                                    String cidade = "",
                                                    String uf = "",
                                                    Int32 cep = 0);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "PesquisaLogradouro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Logradouro> GetPesquisarlogradouro();

        //Cadastro Endereço
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Cadastrar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Int32 GetCadastroEndereco(MEndereco endereco, Int32 identidade);
        
        //DeletarEndereco
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Deletar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetExcluirEndereco(Int32 idendereco,
                                         Int32 idorgunidade);

        //AlterarEndereco
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Alterar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetAlterarEndereco(Endereco endereco, Int32 identidade);

        //PesquisaTipoEndereço
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "PesquisaTipoEndereco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<TipoEndereco>GetPesquisaTipoEndereco();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "importedados", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        void importardadosCsv();

                                          
    }
}
