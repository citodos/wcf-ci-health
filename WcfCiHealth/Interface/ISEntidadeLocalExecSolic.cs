﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfCiHealth
{
    [ServiceContract]
    public interface ISEntidadeLocalExecSolic
    {
       //Cadastro Entidade Local Execução
        [WebInvoke(Method = "POST", UriTemplate = "Cadastrar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Int32 GetCadastroEntLocal(Int32 idlocal,
                                         Int32 identidade,
                                         Int32 identidade_operadora,
                                         Int32 identidade_vinculo,
                                         String tipolocal);

        //Pesquisa Entidade Local Execução
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Pesquisa", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Entidade_LocalExecSolic> GetPesquisaEntLocal(Int32 idlocal);

        //Excluir Entidade Local Execução
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Deletar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetExcluirEntLocal(Int32 idlocal);

        //Alterar Entidade Local Execução
       [OperationContract]
       [WebInvoke(Method = "POST", UriTemplate = "Alterar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
       String GetAlterarEntLocal(Int32 idlocal,
                                        Int32 identidade,
                                        Int32 identidade_operadora,
                                        Int32 identidade_vinculo,
                                        String tipolocal);
    }
}
