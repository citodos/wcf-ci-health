﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WcfCiHealth;

namespace WcfCiHealth
{
   public class SEquipeMov : ISEntidadeEquipeMov
    {
       CIHealthEntities1 context = new CIHealthEntities1();

       //Cadastro Entidade EquipeMov
       public Int32 GetCadastroEntEquipe(Int32 idequipemov,
                                               Int32 identidade_equipe,
                                               Int32 identidade_profissional)
       {
           Entidade_EquipeMov entequ = new Entidade_EquipeMov();
           try
           {
               entequ.idequipemov = idequipemov;
               entequ.identidade_equipe = identidade_equipe;
               entequ.identidade_profissional = identidade_profissional;
               context.Entidade_EquipeMov.Add(entequ);
               context.SaveChanges();

               return idequipemov;
           }
           catch (Exception e)
           {
               return 0;
           }
       }

       //Pesquisa Entidade EquipeMov
       public List<Entidade_EquipeMov> GetPesquisaEntEquipe(Int32 idequipemov)
       {
           if (idequipemov != 0 && idequipemov != null)
           {
               List<Entidade_EquipeMov> entequ = new List<Entidade_EquipeMov>();
               Entidade_EquipeMov entequipe = new Entidade_EquipeMov();

               foreach (var result in context.Entidade_EquipeMov.Where(d => d.idequipemov == idequipemov))
               {
                   //entequ.idequipemov = result.idequipemov;
                   //entequ.identidade_equipe = result.identidade_equipe;
                   //entequ.identidade_profissional = result.identidade_profissional;                   
                   //entequipe.Add(entequ);
               }
               return entequ;
           }
           else
           {
               return null;
           }
       }

       //Exclusão Entidade EquipeMov
       public String GetExcluirEntEquipe(Int32 idequipemov)
       {
           try
           {
               var DeletarEntEquipe = context.Entidade_EquipeMov.FirstOrDefault(p => p.idequipemov == idequipemov);
               context.Entidade_EquipeMov.Remove(DeletarEntEquipe);
               context.SaveChanges();

               return "Exclusão realizada com Sucesso";
           }
           catch (Exception e)
           {
               return "Não Foi Possivel Realizar a Exclusão";
           }
       }

       //Alterar Entidade EquipeMov
       public String GetAlterarEntEquipe(Int32 idequipemov,
                                                 Int32 identidade_equipe,
                                                 Int32 identidade_profissional)
       {
           try
           {
               var atualizaEntEquipe = context.Entidade_EquipeMov.FirstOrDefault(p => p.idequipemov == idequipemov);
               if (atualizaEntEquipe != null)
               {
                   atualizaEntEquipe.idequipemov = idequipemov;
                   atualizaEntEquipe.identidade_equipe = identidade_equipe;
                   atualizaEntEquipe.identidade_profissional = identidade_profissional;                   
                   context.SaveChanges();

                   return "Alteração realizada com Sucesso";
               }
               else
               {
                   return "Esta equipe não consta em nossos registros";
               }
           }
           catch (Exception e)
           {
               return "Não Foi Possivel Realizar a Alteração";
           }
       }

    }
}
