﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WcfCiHealth;

namespace WcfCiHealth
{
   public class SEntidadeJuridica : ISEntidadeJuridica
    {
        CIHealthEntities1 context = new CIHealthEntities1();

        //Cadastro Entidade Juridica
        public Int32 GetCadastroEntJuridica(Int32 identidade,
                                                Int32 contrato,
                                                Int32 limitediasagenda,
                                                Int32 timerparaagendamento,
                                                Int32 identidade_matriz,
                                                DateTime dtultvalidacaonet,                                                
                                                String nomefantasia,
                                                String cnpj,
                                                String inscr_estadual,
                                                String inscr_municipal,
                                                String flg_hub,
                                                String cnes,
                                                String logo,
                                                String codigoibge)
        {
            Entidade_Juridica entjur = new Entidade_Juridica();
            try
            {
                entjur.identidade = identidade;
                entjur.contrato = contrato;
                entjur.limitediasagenda = limitediasagenda;
                entjur.timerparaagendamento = timerparaagendamento;
                entjur.identidade_matriz = identidade_matriz;
                entjur.dtultvalidacaonet = dtultvalidacaonet;                
                entjur.nomefantasia = nomefantasia;
                entjur.cnpj = cnpj;
                entjur.inscr_estadual = inscr_estadual;
                entjur.inscr_municipal = inscr_municipal;
                entjur.flg_hub = flg_hub;
                entjur.cnes = cnes;
                entjur.logo = logo;
                entjur.codigoibge = codigoibge;
                context.Entidade_Juridica.Add(entjur);
                context.SaveChanges();

                return identidade;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

       //Pesquisa Entidade Juridica
       public List<Entidade_Juridica> GetPesquisaEntJuridica(Int32 identidade)
        {
            if (identidade != 0 && identidade != null)
            {
                List<Entidade_Juridica> entjur = new List<Entidade_Juridica>();
                Entidade_Juridica entjurid = new Entidade_Juridica();

                foreach (var result in context.Entidade_Juridica.Where(d => d.identidade == identidade))
                {
                    //entjur.identidade = result.identidade;
                    //entjur.contrato = result.contrato;
                    //entjur.limitediasagenda = result.limitediasagenda;
                    //entjur.timerparaagendamento = result.timerparaagendamento;
                    //entjur.identidade_matriz = result.identidade_matriz;
                    //entjur.dtultvalidacaonet = result.dtultvalidacaonet;                    
                    //entjur.nomefantasia = result.nomefantasia;
                    //entjur.cnpj = result.cnpj;
                    //entjur.inscr_estadual = result.inscr_estadual;
                    //entjur.inscr_municipal = result.inscr_municipal;
                    //entjur.flg_hub = result.flg_hub;
                    //entjur.cnes = result.cnes;
                    //entjur.logo = result.logo;
                    //entjur.codigoibge = result.codigoibge;
                    //entjurid.Add(entjur);
                }

                return entjur;
            }

            else
            {
                return null;
            }
        }

       //Exclusão Entidade Juridica
       public String GetExcluirEntJuridica(Int32 identidade)
       {
           try
           {
               var DeletarEntJuridica = context.Entidade_Juridica.FirstOrDefault(p => p.identidade == identidade);
               context.Entidade_Juridica.Remove(DeletarEntJuridica);
               context.SaveChanges();

               return "Exclusão realizada com Sucesso";
           }
           catch (Exception e)
           {
               return "Não Foi Possivel Realizar a Exclusão";
           }
       }
       
       //Alterar Entidade Juridica
       public String GetAlterarEntJuridica(Int32 identidade,
                                                Int32 contrato,
                                                Int32 limitediasagenda,
                                                Int32 timerparaagendamento,
                                                Int32 identidade_matriz,
                                                DateTime dtultvalidacaonet,                                                
                                                String nomefantasia,
                                                String cnpj,
                                                String inscr_estadual,
                                                String inscr_municipal,
                                                String flg_hub,
                                                String cnes,
                                                String logo,
                                                String codigoibge)
       {
           try
           {
               var atualizaEntJuridica = context.Entidade_Juridica.FirstOrDefault(p => p.identidade == identidade);
               if (atualizaEntJuridica != null)
               {
                   atualizaEntJuridica.identidade = identidade;
                   atualizaEntJuridica.contrato = contrato;
                   atualizaEntJuridica.limitediasagenda = limitediasagenda;
                   atualizaEntJuridica.timerparaagendamento = timerparaagendamento;
                   atualizaEntJuridica.identidade_matriz = identidade_matriz;
                   atualizaEntJuridica.dtultvalidacaonet = dtultvalidacaonet;                   
                   atualizaEntJuridica.nomefantasia = nomefantasia;
                   atualizaEntJuridica.cnpj = cnpj;
                   atualizaEntJuridica.inscr_estadual = inscr_estadual;
                   atualizaEntJuridica.inscr_municipal = inscr_municipal;
                   atualizaEntJuridica.flg_hub = flg_hub;
                   atualizaEntJuridica.cnes = cnes;
                   atualizaEntJuridica.logo = logo;
                   atualizaEntJuridica.codigoibge = codigoibge;
                   context.SaveChanges();

                   return "Alteração realizada com Sucesso";
               }
               else
               {
                   return "Este Local não consta em nossos registros";
               }
           }
           catch (Exception e)
           {
               return "Não Foi Possivel Realizar a Alteração";
           }
       }
    }
}
