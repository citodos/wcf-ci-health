﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Microsoft.Office.Interop;
using System.Data.OleDb;
using System.Data;
using WcfCiHealth.Models;


namespace WcfCiHealth
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SEndereco" in both code and config file together.
    public class SEndereco : ISEndereco
    {
      
         CIHealthEntities1 context = new CIHealthEntities1();

       //CadastroEndereco
         public Int32 GetCadastroEndereco(MEndereco endereco, Int32 identidade) 

       {
           if (endereco.idendereco == 0)
           { 
                Endereco end = new Endereco();
                EnderecoMov mov = new EnderecoMov();
                 try
                 {
                     end.dsendereco = endereco.dsendereco;
                    // end.nrendereco = endereco.nrendereco;
                     end.bairro = endereco.bairro;
                     end.cidade = endereco. cidade;
                     end.uf = endereco.uf;
                     end.cep = endereco.cep;
                     end.idlogradouro = endereco.idlogradouro;
                     //end.complemento = endereco.complemento;
                    

                     context.Enderecoes.Add(end);
                     mov.idendereco = end.idendereco;
                     mov.identidade = identidade;
                     mov.Complemento = endereco.complemento;
                     mov.Numero = endereco.nrendereco;
                     mov.idtipoendereco = endereco.idtipoendereco;
                     context.EnderecoMovs.Add(mov);  
                     context.SaveChanges();


                     endereco.idendereco = end.idendereco;
                     return endereco.idendereco;

                 }
                 catch (Exception e)
                 {
                     return 0;
                 }
           }
           else
           {
               Endereco end = new Endereco();
               EnderecoMov mov = new EnderecoMov();
               try
               {
                   mov.idendereco = endereco.idendereco;
                   mov.identidade = identidade;
                   mov.Numero = endereco.nrendereco;
                   mov.Complemento = endereco.complemento;
                   mov.idtipoendereco = endereco.idtipoendereco;
                   context.EnderecoMovs.Add(mov);
                   context.SaveChanges();



                   return endereco.idendereco;
               }
               catch (Exception e)
               {
                   return 0;
               }

           }
     
       }
          public List<Logradouro> GetPesquisarlogradouro() {
             try { 
             List<Logradouro> logradouro = new List<Logradouro>();
             Logradouro log = new Logradouro();
             foreach (var result in context.Logradouroes.OrderBy(p => p.dslogradouro).ToList())
             {
                  log = new Logradouro();
                  log.dslogradouro = result.dslogradouro;
                  log.idlogradouro = result.idlogradouro;
                  logradouro.Add(log);
             }

             return logradouro;
             }
             catch (Exception e)
             {
                 return null;
             }
            
         }
        //PesquisaEndereco
       public List<Endereco> GetPesquisarEndereco(Int32 idendereco,
                                                         String dsendereco = "",
                                                         String bairro = "",
                                                         String cidade = "",
                                                         String uf = "",
                                                         Int32 cep = 0)

       {

           if (idendereco != 0 && idendereco != null)
           {
               List<Endereco> endereco = new List<Endereco>();
               Endereco end = new Endereco();

               foreach (var result in context.Enderecoes.Join(context.Logradouroes,
                    ende => ende.idlogradouro,
                    log => log.idlogradouro,
                    (ende, log) => new
                    {
                        ende.idendereco,
                        ende.dsendereco,
                        ende.nrendereco,
                        ende.bairro,
                        ende.cidade,
                        ende.uf,
                        ende.cep,
                        ende.idlogradouro,
                        ende.complemento,
                        log.dslogradouro
                    }
                    ).Where(d => d.idendereco == idendereco))
               {
                   end = new Endereco();
                   end.idendereco = result.idendereco;
                   end.dsendereco = result.dsendereco;
                   end.nrendereco = result.nrendereco;
                   end.bairro = result.bairro;
                   end.cidade = result.cidade;
                   end.uf = result.uf;
                   end.cep = result.cep;
                   end.idlogradouro = result.idlogradouro;
                   end.complemento = result.complemento;
                   endereco.Add(end);
               }

               return endereco;
           }
           else  if (dsendereco != "" && dsendereco != null) {
                List<Endereco> endereco = new List<Endereco>();
                Endereco end = new Endereco();

                foreach (var result in context.Enderecoes.Join(context.Logradouroes,
                    ende => ende.idlogradouro,
                    log => log.idlogradouro,
                    (ende, log) => new
                    {
                        ende.idendereco,
                        ende.dsendereco,
                        ende.nrendereco,
                        ende.bairro,
                        ende.cidade,
                        ende.uf,
                        ende.cep,
                        ende.idlogradouro,
                        ende.complemento,
                        log.dslogradouro
                    }
                    ).Where(d => d.dsendereco.Contains("" + dsendereco + "")))
                {
                    end = new Endereco();
                    end.idendereco = result.idendereco;
                    end.dsendereco = result.dsendereco;
                    end.nrendereco =result.nrendereco;
                    end.bairro = result.bairro;
                    end.cidade = result.cidade;
                    end.uf = result.uf;
                    end.cep = result.cep;
                    end.idlogradouro = result.idlogradouro;
                    end.complemento = result.complemento;
                    endereco.Add(end);      
                }
       
                return endereco;
       }
           else if (bairro != "" && bairro != null)
           {
               List<Endereco> endereco = new List<Endereco>();
               Endereco end = new Endereco();

               foreach (var result in context.Enderecoes.Join(context.Logradouroes,
                    ende => ende.idlogradouro,
                    log => log.idlogradouro,
                    (ende, log) => new
                    {
                        ende.idendereco,
                        ende.dsendereco,
                        ende.nrendereco,
                        ende.bairro,
                        ende.cidade,
                        ende.uf,
                        ende.cep,
                        ende.idlogradouro,
                        ende.complemento,
                        log.dslogradouro
                    }
                    ).Where(d => d.bairro.Contains("" + bairro + "")))
               {
                   end = new Endereco();
                   end.idendereco = result.idendereco;
                   end.dsendereco = result.dsendereco;
                   end.nrendereco = result.nrendereco;
                   end.bairro = result.bairro;
                   end.cidade = result.cidade;
                   end.uf = result.uf;
                   end.cep = result.cep;
                   end.idlogradouro = result.idlogradouro;
                   end.complemento = result.complemento;
                   endereco.Add(end);
               }

               return endereco;
           }

          else   if (cidade != "" && cidade != null) {
                List<Endereco> endereco = new List<Endereco>();
                Endereco end = new Endereco();

                foreach (var result in context.Enderecoes.Join(context.Logradouroes,
                    ende => ende.idlogradouro,
                    log => log.idlogradouro,
                    (ende, log) => new
                    {
                        ende.idendereco,
                        ende.dsendereco,
                        ende.nrendereco,
                        ende.bairro,
                        ende.cidade,
                        ende.uf,
                        ende.cep,
                        ende.idlogradouro,
                        ende.complemento,
                        log.dslogradouro
                    }
                    ).Where(d => d.cidade.Contains("" + cidade + "")))
                {
                    end = new Endereco();
                    end.idendereco = result.idendereco;
                    end.dsendereco = result.dsendereco;
                    end.nrendereco =result.nrendereco;
                    end.bairro = result.bairro;
                    end.cidade = result.cidade;
                    end.uf = result.uf;
                    end.cep = result.cep;
                    end.idlogradouro = result.idlogradouro;
                    end.complemento = result.complemento;
                    endereco.Add(end);      
                }
       
                return endereco;
       }
          else   if (uf != "" && uf != null) {
                List<Endereco> endereco = new List<Endereco>();
                Endereco end = new Endereco();

                foreach (var result in context.Enderecoes.Join(context.Logradouroes,
                    ende => ende.idlogradouro,
                    log => log.idlogradouro,
                    (ende, log) => new
                    {
                        ende.idendereco,
                        ende.dsendereco,
                        ende.nrendereco,
                        ende.bairro,
                        ende.cidade,
                        ende.uf,
                        ende.cep,
                        ende.idlogradouro,
                        ende.complemento,
                        log.dslogradouro
                    }
                    ).Where(d => d.uf.Contains("" + uf + "")))
                {
                    end = new Endereco();
                    end.idendereco = result.idendereco;
                    end.dsendereco = result.dsendereco;
                    end.nrendereco =result.nrendereco;
                    end.bairro = result.bairro;
                    end.cidade = result.cidade;
                    end.uf = result.uf;
                    end.cep = result.cep;
                    end.idlogradouro = result.idlogradouro;
                    end.complemento = result.complemento;
                    endereco.Add(end);      
                }
       
                return endereco;
       }
          else   if (cep != 0 && cep != null) {
                List<Endereco> endereco = new List<Endereco>();
                Endereco end = new Endereco();

                foreach (var result in context.Enderecoes.Join(context.Logradouroes,
                    ende => ende.idlogradouro,
                    log => log.idlogradouro,
                    (ende, log) => new {
                    ende.idendereco,
                    ende.dsendereco,
                    ende.nrendereco,
                    ende.bairro,
                    ende.cidade,
                    ende.uf,
                    ende.cep,
                    ende.idlogradouro,
                    ende.complemento,
                    log.dslogradouro
                        }
                    ).Where(d => d.cep == cep))
                {
                    end = new Endereco();
                    end.idendereco = result.idendereco;
                    end.dsendereco = result.dsendereco;
                    end.nrendereco = result.nrendereco;
                    end.bairro = result.bairro;
                    end.cidade = result.cidade;
                    end.uf = result.uf;
                    end.cep = result.cep;
                    end.idlogradouro = result.idlogradouro;
                    end.complemento = result.complemento;
                    end.Logradouro = new Logradouro();
                    end.Logradouro.dslogradouro = result.dslogradouro;
                    endereco.Add(end);
                }
       
                return endereco;
       }
          else {
              return null;
          }
       }

        //excluirEndereco
          public String GetExcluirEndereco(Int32 idendereco,
                                                    Int32 idorgunidade)
        {
            try
            {
                //var Deletarunidade = context.EnderecoMovs.FirstOrDefault(p => p.idendereco == idendereco && p.idorgunidade == idorgunidade);
                //context.EnderecoMovs.Remove(Deletarunidade);
                context.SaveChanges();

                return "Exclusão realizada com Sucesso";
            }
            catch (Exception e)
            {
                return "Não Foi Possivel Realizar a Exclusão";
            }
        }

        //AlterarEndereco
          public String GetAlterarEndereco(Endereco endereco, Int32 identidade) 
    {
        try
              {
                  var atualizaendereco = context.Enderecoes.FirstOrDefault(p => p.idendereco == endereco.idendereco);
                  if (atualizaendereco != null)
                  {

                      atualizaendereco.idendereco = endereco.idendereco;
                      atualizaendereco.dsendereco = endereco.dsendereco;
                      atualizaendereco.nrendereco = endereco.nrendereco;
                      atualizaendereco.bairro = endereco.bairro;
                      atualizaendereco.cidade = endereco.cidade;
                      atualizaendereco.uf = endereco.uf;
                      atualizaendereco.cep = endereco.cep;
                      atualizaendereco.idlogradouro = endereco.idlogradouro;
                      atualizaendereco.complemento = endereco.complemento; 
                      context.SaveChanges();
                      return "Alteração realizada com Sucesso";
                  }
                  else
                  {
                      return "Esta empresa não consta em nossos registros";
                  }

              }
              catch (Exception e)
              {
                  return "Não Foi Possivel Realizar a Alteração";
              }
          
      }
          public List<TipoEndereco> GetPesquisaTipoEndereco()
          {
              try
              {
                  List<TipoEndereco> tipoendereco = new List<TipoEndereco>();
                  TipoEndereco TE = new TipoEndereco();                  
                  foreach (var result in context.TipoEnderecoes.OrderBy(p => p.dstipoendereco).ToList())
                  {
                      TE = new TipoEndereco();
                      TE.idtipoendereco = result.idtipoendereco;
                      TE.dstipoendereco = result.dstipoendereco;                      
                      tipoendereco.Add(TE);
                  }
                  return tipoendereco;
              }
              catch (Exception e)
              {
                  return null;
              }

          }

          public void importardadosCsv()
          {
              //context.CommandTimeout = 100000;
              string arquivo = "C:/Users/CI/Desktop/Cep/cep.xlsx";
              string strConexao = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=0\"", arquivo);
              OleDbConnection conn = new OleDbConnection(strConexao);
              conn.Open();
              DataTable dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
              //Cria o objeto dataset para receber o conteúdo do arquivo Excel
              DataSet output = new DataSet();
              DataTable dt2 = new DataTable();
              foreach (DataRow row in dt.Rows)
              {
                  // obtem o noma da planilha corrente
                  string sheet = row["TABLE_NAME"].ToString();
                  // obtem todos as linhas da planilha corrente
                  OleDbDataAdapter da = new OleDbDataAdapter("Select * From [" + sheet + "]", conn);
                  da.Fill(output);

                  dt2 = output.Tables[0];
             }
              Logradouro log = new Logradouro();
              Endereco end = new Endereco();
              foreach (DataRow row in dt2.Rows)
              {
                  string verifica = row[1].ToString();
                 // log = new Logradouro();
                 // if (row[1].ToString() != "Tipo_Logradouro" && row[1].ToString() != "") {
                 //     var existe = context.Logradouroes.FirstOrDefault(p => p.dslogradouro == verifica);
                 //     if (existe == null) { 
                 //     log.dslogradouro = row[1].ToString();
                 //     context.Logradouroes.Add(log);
                 //     context.SaveChanges();
                 //     existe = null;
                 //     }
                 //}
                  Int32 cep = Convert.ToInt32(row[0].ToString());
                  Logradouro exist = context.Logradouroes.FirstOrDefault(p => p.dslogradouro == verifica);
                  Endereco existend = null; //context.Enderecoes.FirstOrDefault(p => p.cep == cep);
                  if (existend == null)
                  { 
                  end.cep = Convert.ToInt32(row[0].ToString());
                  if (exist != null)
                  {
                      end.idlogradouro = Convert.ToInt32(exist.idlogradouro);
                  }
                  else
                  {
                      end.idlogradouro = 213;
                  }
                  end.dsendereco = row[2].ToString();
                  end.bairro = row[3].ToString();
                  end.cidade = row[4].ToString();
                  end.uf = row[5].ToString();

                  context.Enderecoes.Add(end);
                  context.SaveChanges();


                  }

              }
              end = null;
            

             

          }
          
    }
    
}
