﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfCiHealth;
using WcfCiHealth.Models;

namespace WcfCiHealth
{
   public class SEntidade : ISEntidade
    {
        CIHealthEntities1 context = new CIHealthEntities1();

        //Cadastro Entidade Juridica
        public long GetCadastroEntidadeJuridica(MEntidade entidade, MEntidadeJuridica entidadejuridica, MLocaisExec locaisExec = null, MEndereco endereco = null)
        {
            try
            {
                if (entidade.idtipo == "")
                {
                    return 0;
                }
                     EnderecoMov mov = new EnderecoMov();
                     Entidade ent = new Entidade();
                     Entidade_Juridica entjur = new Entidade_Juridica();
                     Entidade_LocalExecSolic locais = new Entidade_LocalExecSolic();
                     ent.idtipo = entidade.idtipo;
                     ent.identidade_vinculo = entidade.identidade_vinculo;
                     ent.nome = entidade.nome;
                     ent.fone = entidade.fone;
                     ent.email = entidade.email;
                     ent.celular = entidade.celular;
                     ent.flg_status = entidade.flg_status;
                     ent.nomecontato = entidade.nomecontato;
                     ent.dtcadastro = DateTime.Now;
                     // ent.dtexclusao = dtexclusao;               
                     context.Entidades.Add(ent);
                     context.SaveChanges();
                     long identidade = ent.identidade;
                    
                     entjur.identidade = ent.identidade;
                     entjur.contrato = entidadejuridica.contrato;
                     entjur.limitediasagenda = entidadejuridica.limitediasagenda;
                     entjur.timerparaagendamento = entidadejuridica.timerparaagendamento;
                     entjur.identidade_matriz = entidadejuridica.identidade_matriz;
                     entjur.dtultvalidacaonet = entidadejuridica.dtultvalidacaonet;
                     entjur.nomefantasia = entidadejuridica.nomefantasia;
                     entjur.cnpj = entidadejuridica.cnpj;
                     entjur.inscr_estadual = entidadejuridica.inscr_estadual;
                     entjur.inscr_municipal = entidadejuridica.inscr_municipal;
                     entjur.flg_hub = entidadejuridica.flg_hub;
                     entjur.cnes = entidadejuridica.cnes;
                     entjur.logo = entidadejuridica.logo;
                     entjur.codigoibge = entidadejuridica.codigoibge;
                     
                     context.Entidade_Juridica.Add(entjur);
                     context.SaveChanges();
                switch (entidade.idtipo)
	        {
                case "EM": 
                                  
                    break;

                case "CV" :
                              
                    break;

                case "HB" :
                   
                    break;

                case "UN" :
                    if (entidade.identidade_vinculo == 0)
                    {
                       return 0; 
                    }
                    
                    break;
                case "LE":
                    locais.identidade = ent.identidade;
                    locais.identidade_operadora = (long)locaisExec.identidade_operadora;
                    locais.codoperadora = locaisExec.codoperadora;
                    context.Entidade_LocalExecSolic.Add(locais);

                        //Gravando endereço do Local de Execução
                        //verificando se o idendereco é = 0 se for grava um endereco novo
                    if (endereco.idendereco == 0)
                    {
                        mov.idendereco = gravarend(endereco);
                        mov.identidade = ent.identidade;
                        mov.Numero = endereco.nrendereco;
                        mov.Complemento = endereco.complemento;
                        mov.idtipoendereco = endereco.idtipoendereco;
                        context.EnderecoMovs.Add(mov);
                    }
                        //senão usa o id que veio de requisição para gravar na tb enderecomov
                    else
                    {
                        mov.idendereco = endereco.idendereco;
                        mov.identidade = ent.identidade;
                        mov.Numero = endereco.nrendereco;
                        mov.idtipoendereco = endereco.idtipoendereco;
                        mov.Complemento = endereco.complemento;
                        
                        context.EnderecoMovs.Add(mov);
                        
                    }

                    break;
                case "LC":
                    locais.identidade = ent.identidade;
                    locais.identidade_operadora = (int)locaisExec.identidade_operadora;
                    locais.codoperadora = locaisExec.codoperadora;

             
                    context.Entidade_LocalExecSolic.Add(locais);

                    if (endereco.idendereco == 0)
                    {
                        mov.idendereco = gravarend(endereco);
                        mov.identidade = ent.identidade;
                        mov.Numero = endereco.nrendereco;
                        mov.Complemento = endereco.complemento;
                        mov.idtipoendereco = endereco.idtipoendereco;
                        context.EnderecoMovs.Add(mov);
                    }
                    //senão usa o id que veio de requisição para gravar na tb enderecomov
                    else
                    {
                        mov.idendereco = endereco.idendereco;
                        mov.identidade = ent.identidade;
                        mov.Numero = endereco.nrendereco;
                        mov.Complemento = endereco.complemento;
                        mov.idtipoendereco = endereco.idtipoendereco;
                        context.EnderecoMovs.Add(mov);
                       
                    }
                    break;
              default:
                    break;
            }
                    
               
                context.SaveChanges();
                return (long)ent.identidade;
            }
            catch (Exception e)
            {
              return 0;
            }
        }
        int gravarend(MEndereco end)
        {
           
                Endereco ender = new Endereco();
               
                 try
                 {
                     ender.dsendereco = end.dsendereco;
                     ender.nrendereco = end.nrendereco;
                     ender.bairro = end.bairro;
                     ender.cidade = end. cidade;
                     ender.uf = end.uf;
                     ender.cep = end.cep;
                     ender.idlogradouro = end.idlogradouro;
                     ender.complemento = end.complemento;

                     context.Enderecoes.Add(ender);
                       
                    
                     return ender.idendereco;

                     

                 }
                 catch (Exception e)
                 {
                     return 0;
                 }
                     
                                        

        }

        //Cadastro Entidade Fisica
        public long GetCadastroEntidadeFisica(MEntidade entidade, Entidade_Fisica entidadefisica)
        {
            Entidade ent = new Entidade();
            Entidade_Fisica entfis = new Entidade_Fisica();
            try
       {
                switch (entidade.idtipo)
       {
           case "PC" :
           ent.idtipo = entidade.idtipo;
           ent.identidade_vinculo = entidade.identidade_vinculo;

           ent.nome = entidade.nome;
           ent.fone = entidade.fone;
           ent.email = entidade.email;
           ent.celular = entidade.celular;
           ent.flg_status = entidade.flg_status;
           ent.nomecontato = entidade. nomecontato;
           ent.dtcadastro = DateTime.Now;
               // ent.dtexclusao = dtexclusao;               

           Int32 identidade = (Int32)ent.identidade;
               
               entfis.identidade = entidadefisica.identidade;
               entfis.idestcivil = entidadefisica.idestcivil;
               entfis.idcor = entidadefisica.idcor;
               entfis.cpf = entidadefisica.cpf;
               entfis.dtnascimento = entidadefisica.dtnascimento;
               entfis.sexo = entidadefisica.sexo;
               entfis.pai = entidadefisica.pai;
               entfis.mae = entidadefisica.mae;
               entfis.naturalidade = entidadefisica.naturalidade;
               entfis.rg = entidadefisica.rg;
               entfis.cns = entidadefisica.cns;
               entfis.profissao = entidadefisica.profissao;
               
                context.Entidades.Add(ent);
                context.Entidade_Fisica.Add(entfis);                               

                break;                             

           case "EQ":
            ent.idtipo = entidade.idtipo;
            ent.identidade_vinculo = entidade.identidade_vinculo;

            ent.nome = entidade.nome;
            ent.fone = entidade.fone;
            ent.email = entidade.email;
            ent.celular = entidade.celular;
            ent.flg_status = entidade.flg_status;
            ent.nomecontato = entidade. nomecontato;
            ent.dtcadastro = DateTime.Now;
                   // ent.dtexclusao = dtexclusao;                              
               
                   entfis.identidade = entidadefisica.identidade;
                   entfis.idestcivil = entidadefisica.idestcivil;
                   entfis.idcor = entidadefisica.idcor;
                   entfis.cpf = entidadefisica.cpf;
                   entfis.dtnascimento = entidadefisica.dtnascimento;
                   entfis.sexo = entidadefisica.sexo;
                   entfis.pai = entidadefisica.pai;
                   entfis.mae = entidadefisica.mae;
                   entfis.naturalidade = entidadefisica.naturalidade;
                   entfis.rg = entidadefisica.rg;
                   entfis.cns = entidadefisica.cns;
                   entfis.profissao = entidadefisica.profissao;
                   
                   context.Entidades.Add(ent);
                   context.Entidade_Fisica.Add(entfis);               

                   break;
       
            default :
                break;
        }
               context.SaveChanges();
               return (long)ent.identidade;
           }        
           catch (Exception e)
           {
               return 0;
           }
       }                          

       //Pesquisa entidade juridica
        public List <Entidade> GetPesquisaEntidadeJuridica(Int32 identidade)
        {
            Entidade entid = new Entidade();               
            MEntidadeJuridica mjur;

            if (identidade != 0 && identidade != null)
            {     
                Entidade ent = new Entidade();
                Entidade_LocalExecSolic locais = new Entidade_LocalExecSolic();
                List<Entidade> entidadelist = new List<Entidade>();
                Entidade_Juridica jur = new Entidade_Juridica();
                ent.Entidade_Juridica = jur;
                EnderecoMov mov = new EnderecoMov();

                foreach (var result in context.Entidades.Include("Entidade_Juridica").Include("EnderecoMovs.Endereco").Include("Entidade_LocalExecSolic").Where(d => d.identidade == identidade).ToList())
                {
                    ent.idtipo = result.idtipo;
                    ent.identidade = result.identidade;
                    ent.identidade_vinculo = result.identidade_vinculo;
                    ent.nome = result.nome;
                    ent.fone = result.fone;
                    ent.email = result.email;
                    ent.celular = result.celular;
                    ent.flg_status = result.flg_status;
                    ent.nomecontato = result.nomecontato;
                    ent.dtcadastro = result.dtcadastro;
                    ent.dtexclusao = result.dtexclusao;                                  
                    
                    Entidade_Juridica entjur = new Entidade_Juridica();
                    Entidade_Juridica entjurid = new Entidade_Juridica();                                                             

                     entjur.contrato = result.Entidade_Juridica.contrato;
                     entjur.limitediasagenda = result.Entidade_Juridica.limitediasagenda;
                     entjur.timerparaagendamento = result.Entidade_Juridica.timerparaagendamento;
                     entjur.identidade_matriz = result.Entidade_Juridica.identidade_matriz;
                     entjur.dtultvalidacaonet = result.Entidade_Juridica.dtultvalidacaonet;
                     entjur.nomefantasia = result.Entidade_Juridica.nomefantasia;
                     entjur.cnpj = result.Entidade_Juridica.cnpj;
                     entjur.inscr_estadual = result.Entidade_Juridica.inscr_estadual;
                     entjur.inscr_municipal = result.Entidade_Juridica.inscr_municipal;
                     entjur.flg_hub = result.Entidade_Juridica.flg_hub;
                     entjur.cnes = result.Entidade_Juridica.cnes;
                     entjur.logo = result.Entidade_Juridica.logo;
                     entjur.codigoibge = result.Entidade_Juridica.codigoibge;
                    foreach(var movend in result.EnderecoMovs.Where(m=> m.identidade ==identidade )){
                        mov = new EnderecoMov();
                        mov.identidade = movend.identidade;
                        mov.Numero = movend.Numero;
                        mov.idtipoendereco = movend.idtipoendereco;
                        mov.idendereco = movend.idendereco;
                        mov.Endereco = new Endereco();
                        mov.Endereco.bairro = movend.Endereco.bairro;
                        mov.Endereco.cep = movend.Endereco.cep;
                        mov.Endereco.cidade = movend.Endereco.cidade;
                        mov.Complemento = movend.Complemento;
                        mov.Endereco.dsendereco = movend.Endereco.dsendereco;
                        mov.Endereco.idendereco = movend.Endereco.idendereco;
                        mov.Endereco.Logradouro = new Logradouro();
                        mov.Endereco.Logradouro.dslogradouro = movend.Endereco.Logradouro.dslogradouro;
                        mov.Endereco.Logradouro.idlogradouro = movend.Endereco.Logradouro.idlogradouro;
                        mov.Endereco.uf = movend.Endereco.uf;


                        ent.EnderecoMovs.Add(mov);
                    }

                    if (ent.idtipo == "LE" || ent.idtipo == "LS")
                    {
                        foreach (var loc in result.Entidade_LocalExecSolic.Where(m => m.identidade == identidade)) {
                            locais = new Entidade_LocalExecSolic();
                            locais.idlocal = loc.idlocal;
                            locais.identidade_operadora = loc.identidade_operadora;
                            locais.codoperadora = loc.codoperadora;

                            ent.Entidade_LocalExecSolic.Add(locais);

                        }

                    }
                     ent.Entidade_Juridica = entjur;


                     entidadelist.Add(ent);
                    
                }
                return entidadelist;
            }
            else
            {
                return null;
            }
        }

       //pesquisa entidade fisica
        public Entidade GetPesquisaEntidadeFisica(Int32 identidade,
                                                             String idtipo)
         {
            Entidade entid = new Entidade();               
            MFisica mfis;

            if (identidade != 0 && identidade != null)
            {
                Entidade ent = new Entidade();
                //ent.EntidadeFisica = new List<MFisica>();

                foreach (var result in context.Entidades.Include("Entidade_Fisica").Where(d => d.identidade == identidade).ToList())
                {
                    ent.identidade = result.identidade;
                    ent.identidade_vinculo = result.identidade_vinculo;
                    ent.nome = result.nome;
                    ent.fone = result.fone;
                    ent.email = result.email;
                    ent.celular = result.celular;
                    ent.flg_status = result.flg_status;
                    ent.nomecontato = result.nomecontato;
                    ent.dtcadastro = result.dtcadastro;
                    ent.dtexclusao = result.dtexclusao;       

                    Entidade_Fisica entfis = new Entidade_Fisica();
                    Entidade_Fisica entFisica = new Entidade_Fisica();

                    entfis.identidade = result.Entidade_Fisica.identidade;
                    entfis.idestcivil = result.Entidade_Fisica.idestcivil;
                    entfis.idcor = result.Entidade_Fisica.idcor;
                    entfis.cpf = result.Entidade_Fisica.cpf;
                    entfis.dtnascimento = result.Entidade_Fisica.dtnascimento;
                    entfis.sexo = result.Entidade_Fisica.sexo;
                    entfis.pai = result.Entidade_Fisica.pai;
                    entfis.mae = result.Entidade_Fisica.mae;
                    entfis.naturalidade = result.Entidade_Fisica.naturalidade;
                    entfis.rg = result.Entidade_Fisica.rg;
                    entfis.cns = result.Entidade_Fisica.cns;
                    entfis.profissao = result.Entidade_Fisica.profissao;
                    ent.Entidade_Fisica = entfis;
                }
                return ent;
            }
            return null;
       }

       //Exclusao entidade
        public String GetExcluirEntidade(Int32 identidade)
        {
            try
              {
                  var atualizaEntidade = context.Entidades.FirstOrDefault(p => p.identidade == identidade);
                  if (atualizaEntidade != null)
                  {
                      atualizaEntidade.dtexclusao = DateTime.Now;
                      context.SaveChanges();
                  }
                  return "Exclusão Realizada com Sucesso";
              }
         catch (Exception e)
            {
                return "Não Foi Possivel Realizar a Exclusão";
            }
        }

       //Alterar Entidade
        public String GetAlterarEntidade(MEntidade entidade, MEntidadeJuridica entidadejuridica, MLocaisExec locaisExec = null, MEndereco endereco = null)
                                        
        {
         try
              {
                  var atualizaEntidade = context.Entidades.FirstOrDefault(p => p.identidade == entidade.identidade);
                  var atualizaentJuridica = context.Entidade_Juridica.FirstOrDefault(j => j.identidade == entidade.identidade);
                  var atualizalocalexec = context.Entidade_LocalExecSolic.FirstOrDefault(l => l.identidade == entidade.identidade);
                  var atualizaEndMov = context.EnderecoMovs.FirstOrDefault(m => m.idendereco == endereco.idendereco && m.identidade == entidade.identidade);
                  if (atualizaEntidade != null)
                  {

                      atualizaEntidade.identidade = entidade.identidade;
                      atualizaEntidade.identidade_vinculo = entidade.identidade_vinculo;
                      atualizaEntidade.nome = entidade.nome;
                      atualizaEntidade.fone = entidade.fone;
                      atualizaEntidade.email = entidade.email;
                      atualizaEntidade.celular = entidade.celular;
                      atualizaEntidade.flg_status = entidade.flg_status;
                      atualizaEntidade.nomecontato = entidade.nomecontato;
                      atualizaEntidade.dtcadastro = DateTime.Now;

                      atualizaentJuridica.identidade = entidade.identidade;
                      atualizaentJuridica.contrato = entidadejuridica.contrato;
                      atualizaentJuridica.limitediasagenda = entidadejuridica.limitediasagenda;
                      atualizaentJuridica.timerparaagendamento = entidadejuridica.timerparaagendamento;
                      atualizaentJuridica.identidade_matriz = entidadejuridica.identidade_matriz;
                      atualizaentJuridica.dtultvalidacaonet = entidadejuridica.dtultvalidacaonet;
                      atualizaentJuridica.nomefantasia = entidadejuridica.nomefantasia;
                      atualizaentJuridica.cnpj = entidadejuridica.cnpj;
                      atualizaentJuridica.inscr_estadual = entidadejuridica.inscr_estadual;
                      atualizaentJuridica.inscr_municipal = entidadejuridica.inscr_municipal;
                      atualizaentJuridica.flg_hub = entidadejuridica.flg_hub;
                      atualizaentJuridica.cnes = entidadejuridica.cnes;
                      atualizaentJuridica.logo = entidadejuridica.logo;
                      atualizaentJuridica.codigoibge = entidadejuridica.codigoibge;

                      switch (entidade.idtipo)
                      {
                          case "EM":

                              break;

                          case "CV":

                              break;

                          case "HB":

                              break;

                          case "UN":
                              if (entidade.identidade_vinculo == 0)
                              {
                                  return "";
                              }

                              break;
                          case "LE":
                              atualizalocalexec.identidade = entidade.identidade;
                              atualizalocalexec.identidade_operadora = (int)locaisExec.identidade_operadora;
                             
                              
                              //Gravando endereço do Local de Execução
                              //verificando se o idendereco é = 0 se for grava um endereco novo
                              if (endereco.idendereco == 0)
                              {
                                  atualizaEndMov.idendereco = gravarend(endereco);
                                  atualizaEndMov.identidade = entidade.identidade;
                                  atualizaEndMov.Numero = endereco.nrendereco;
                                  atualizaEndMov.Complemento = endereco.complemento;
                                  
                              }
                              //senão usa o id que veio de requisição para gravar na tb enderecomov
                              else
                              {
                                  atualizaEndMov.idendereco = endereco.idendereco;
                                  atualizaEndMov.identidade = entidade.identidade;
                                  atualizaEndMov.Numero = endereco.nrendereco;
                                  atualizaEndMov.Complemento = endereco.complemento;
                                 

                              }

                              break;
                          case "LC":
                             atualizalocalexec.identidade = entidade.identidade;
                             atualizalocalexec.identidade_operadora = (int)locaisExec.identidade_operadora;
                         
                           
                              if (endereco.idendereco == 0)
                              {
                                  atualizaEndMov.idendereco = gravarend(endereco);
                                  atualizaEndMov.identidade = entidade.identidade;
                                  atualizaEndMov.Numero = endereco.nrendereco;
                                  atualizaEndMov.Complemento = endereco.complemento;
                                  
                              }
                              //senão usa o id que veio de requisição para gravar na tb enderecomov
                              else
                              {
                                  atualizaEndMov.idendereco = endereco.idendereco;
                                  atualizaEndMov.identidade = entidade.identidade;
                                  atualizaEndMov.Numero = endereco.nrendereco;
                                  atualizaEndMov.Complemento = endereco.complemento;
                                  

                              }
                              break;
                          default:
                              break;
                      }


                      context.SaveChanges();
                      
                      return "Alteração realizada com Sucesso";
                  }
                  else
                {
                      return "Este Local não consta em nossos registros";
                  }
              }
              catch (Exception e)
              {
                  return "Não Foi Possivel Realizar a Alteração";
              }
        }

        public List<Entidade> GetPesquisarConvenio()
        {
            try
            {
                List<Entidade> entidade = new List<Entidade>();
                Entidade log = new Entidade();
                //var result = context.Entidades.Where(p=> p.idtipo == "CV").ToList();
                foreach (var result in context.Entidades.Where(p => p.idtipo == "CV").OrderBy(p => p.nome).ToList())
                {
                    log = new Entidade();
                    log.nome = result.nome;
                    log.identidade = result.identidade;
                    entidade.Add(log);
                }

                return entidade;
            }
            catch (Exception e)
            {
                return null;
            }
        
        }
      
        public List<Entidade> GetPesquisaEntidadeJuriCampos(String Coluna, String Conteudo, String idTipo,int idvinculo = 0) {
            try
            {
                List<Entidade> entidaderesult = new List<Entidade>();
                List<Entidade> entidade = new List<Entidade>();
                Entidade log = new Entidade();
                Entidade_Juridica Jur = new Entidade_Juridica();
                
                switch (Coluna)
                {
                    case "Nome":

                        var result = context.Entidades.Include("Entidade_Juridica").Where(p => p.idtipo == idTipo && p.nome.Contains("" + Conteudo + "") && p.identidade_vinculo == idvinculo ).ToList();
                      entidaderesult = result.ToList(); 
                       
                      break;

                    case "CNPJ":
                      var resultentJuri = context.Entidade_Juridica.Where(p => p.cnpj == Conteudo).ToList();
                      foreach (var id in resultentJuri.ToList())
                      {
                          var resultCnpj = context.Entidades.Where(p => p.identidade == id.identidade && p.identidade_vinculo == idvinculo);
                         entidaderesult = resultCnpj.ToList();
                      }
                       
                        break;

                    case "NomeFantasia":
                        List<int> ids = new List<int>();
                        var resultentJur = context.Entidade_Juridica.Where(p => p.nomefantasia.Contains("" + Conteudo + "")).ToList();
                      foreach (var id in resultentJur.ToList())
                      {
                          foreach (var resultCnpj in context.Entidades.Where(p => p.identidade == id.identidade && p.idtipo == idTipo && p.identidade_vinculo == idvinculo))
                          { 
                          entidaderesult.Add(resultCnpj);
                          }
                      }
                       
                        break;

                    default:
                        break;
                }

                foreach (var result in entidaderesult.ToList())
                {
                    log = new Entidade();
                    log.nome = result.nome;
                    log.identidade = result.identidade;
                    log.Entidade_Juridica = new Entidade_Juridica();
                    log.Entidade_Juridica.cnpj = result.Entidade_Juridica.cnpj;
                    log.Entidade_Juridica.nomefantasia = result.Entidade_Juridica.nomefantasia; 
                    entidade.Add(log);
                }

                return entidade;
            }
            catch (Exception e)
            {
                return null;
            }

        
        }


       
    }           
    }
