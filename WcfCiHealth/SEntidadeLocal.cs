﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WcfCiHealth;

namespace WcfCiHealth
{
    public class SEntidadeLocal : ISEntidadeLocalExecSolic 
    {
        CIHealthEntities1 context = new CIHealthEntities1();

        //Cadastro Entidade Local Execução
        public Int32 GetCadastroEntLocal(Int32 idlocal,
                                                Int32 identidade,
                                                Int32 identidade_operadora,
                                                Int32 identidade_vinculo,
                                                String tipolocal)
        {
            Entidade_LocalExecSolic entloc = new Entidade_LocalExecSolic();
            try
            {
                entloc.idlocal = idlocal;
                entloc.identidade = identidade;
                entloc.identidade_operadora = identidade_operadora;
               
                context.Entidade_LocalExecSolic.Add(entloc);
                context.SaveChanges();

                return idlocal;
            }
            catch (Exception e)
            {
                return 0;
          }
        }

        //Pesquisa Entidade Local Execução
        public List<Entidade_LocalExecSolic> GetPesquisaEntLocal(Int32 idlocal)
        {
            if (idlocal != 0 && idlocal != null)
            {
                List<Entidade_LocalExecSolic> entloc = new List<Entidade_LocalExecSolic>();
                Entidade_LocalExecSolic entlocal = new Entidade_LocalExecSolic();

                foreach (var result in context.Entidade_LocalExecSolic.Where(d => d.idlocal == idlocal))
                {
                    //entloc.idlocal = result.idlocal;
                    //entloc.identidade = result.identidade;
                    //entloc.identidade_operadora = result.identidade_operadora;
                    //entloc.identidade_vinculo = result.identidade_vinculo;
                    //entloc.tipolocal = result.tipolocal;                                        
                    //entlocal.Add(entloc);
                }
                return entloc;
            }
            else
            {
                return null;
            }
        }

        //Excluir Entidade Local Execução
        public String GetExcluirEntLocal(Int32 idlocal)
        {
            try
            {
                var DeletarEntLocal = context.Entidade_LocalExecSolic.FirstOrDefault(p => p.idlocal == idlocal);
                context.Entidade_LocalExecSolic.Remove(DeletarEntLocal);
                context.SaveChanges();

                return "Exclusão realizada com Sucesso";
            }
            catch (Exception e)
            {
                return "Não Foi Possivel Realizar a Exclusão";
            }
        }

        //Alterar Entidade Local Execução       
        public String GetAlterarEntLocal(Int32 idlocal,
                                                 Int32 identidade,
                                                 Int32 identidade_operadora,
                                                 Int32 identidade_vinculo,
                                                 String tipolocal)                                                
        {
            try
            {
                var atualizaEntLocal = context.Entidade_LocalExecSolic.FirstOrDefault(p => p.idlocal == idlocal);
                if (atualizaEntLocal != null)
                {
                    atualizaEntLocal.idlocal = idlocal;
                    atualizaEntLocal.identidade = identidade;
                    atualizaEntLocal.identidade_operadora = identidade_operadora;
                                       
                    context.SaveChanges();

                    return "Alteração realizada com Sucesso";
                }
                else
                {
                    return "Este Local não consta em nossos registros";
                }
            }
            catch (Exception e)
            {
                return "Não Foi Possivel Realizar a Alteração";
            }
        }
    }
}
