﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfCiHealth.Interface;

namespace WcfCiHealth
{
    public class SPaciente : ISPaciente
    {
        CIHealthEntities context = new CIHealthEntities();

        //Cadastro Pacientes
        public Int32 GetCadastroPaciente(Int32 idpaciente,
                                             Int32 idrofissao,
                                             Int32 idestcivil, 
                                             Int32 idcor,
                                             Int32 cpf,
                                             Int32 idorg,
                                             DateTime dtcadastro,
                                             DateTime dtnascimento,
                                             DateTime dtexclusao,
                                             String paciente,
                                             String sexo,
                                             String pai,
                                             String mae, 
                                             String naturalidade,
                                             String rg,
                                             String cns)
        {
            Paciente pac = new Paciente();
            try
            {
                pac.idpaciente = idpaciente;
                pac.idrofissao = idrofissao;
                pac.idestcivil = idestcivil;
                pac.idcor = idcor;
                pac.cpf = cpf;
                pac.idorg = idorg;
                pac.dtcadastro = dtcadastro;
                pac.dtnascimento = dtnascimento;
                pac.dtexclusao = dtexclusao;
                pac.paciente1 = paciente;
                pac.sexo = sexo;
                pac.pai = pai;
                pac.mae = mae;
                pac.naturalidade = naturalidade;
                pac.rg = rg;
                pac.cns = cns;
                context.Pacientes.Add(pac);
                context.SaveChanges();

                return idpaciente;

            }
            catch (Exception e)
            {
                return 0;
            }

        } 

        //Pesquisa Paciente
        public List<Paciente> GetPesquisaPaciente(Int32 idpaciente)
        {
            if (idpaciente != 0 && idpaciente != null)
            {
                List<Paciente> pacient = new List<Paciente>();
                Paciente pac = new Paciente();

                foreach (var result in context.Pacientes.Where(d => d.idpaciente == idpaciente))
                {
                    pac.idpaciente = result.idpaciente;
                    pac.idrofissao = result.idrofissao;
                    pac.idestcivil = result.idestcivil;
                    pac.idcor = result.idcor;
                    pac.cpf = result.cpf;
                    pac.idorg = result.idorg;
                    pac.dtcadastro = result.dtcadastro;
                    pac.dtnascimento = result.dtnascimento;
                    pac.dtexclusao = result.dtexclusao;
                    pac.paciente1 = result.paciente1;
                    pac.sexo = result.sexo;
                    pac.pai = result.pai;
                    pac.mae = result.mae;
                    pac.naturalidade = result.naturalidade;
                    pac.rg = result.rg;
                    pac.cns = result.cns;
                    pacient.Add(pac);
                }

                return pacient;
            }

            else
            {
                return null;

            }
        }
        //Exclusão Paciente
        public String GetExcluirPaciente(Int32 idpaciente)
        {
            try
            {
                var DeletarPaciente = context.Pacientes.FirstOrDefault(p => p.idpaciente == idpaciente);
                context.Pacientes.Remove(DeletarPaciente);
                context.SaveChanges();

                return "Exclusão realizada com Sucesso";
            }
            catch (Exception e)
            {
                return "Não Foi Possivel Realizar a Exclusão";
            }
        }

     //Alterar Locais
        public String GetAlterarPaciente(Int32 idpaciente,
                                             Int32 idrofissao,
                                             Int32 idestcivil, 
                                             Int32 idcor,
                                             Int32 cpf,
                                             Int32 idorg,
                                             DateTime dtcadastro,
                                             DateTime dtnascimento,
                                             DateTime dtexclusao,
                                             String paciente,
                                             String sexo,
                                             String pai,
                                             String mae, 
                                             String naturalidade,
                                             String rg,
                                             String cns)
                                        
        {
         try
              {
                  var atualizaPaciente = context.Pacientes.FirstOrDefault(p => p.idpaciente == idpaciente);
                  if (atualizaPaciente != null)
                  {
                     
                      atualizaPaciente.idpaciente = idpaciente;
                      atualizaPaciente.idrofissao = idrofissao;
                      atualizaPaciente.idestcivil = idestcivil;
                      atualizaPaciente.idcor = idcor;
                      atualizaPaciente.cpf = cpf;
                      atualizaPaciente.dtcadastro = dtcadastro;
                      atualizaPaciente.dtnascimento = dtnascimento;
                      atualizaPaciente.dtexclusao = dtexclusao;
                      atualizaPaciente.paciente1 = paciente; 
                      atualizaPaciente.sexo = sexo;
                      atualizaPaciente.pai = pai;
                      atualizaPaciente.mae = mae;
                      atualizaPaciente.naturalidade = naturalidade;
                      atualizaPaciente.rg = rg;
                      atualizaPaciente.cns = cns;               
                      context.SaveChanges();
                      
                      return "Alteração realizada com Sucesso";
                  }
                  else
                  {
                      return "Este Local não consta em nossos registros";
                  }

              }
              catch (Exception e)
              {
                  return "Não Foi Possivel Realizar a Alteração";
              }
          

        }
    }
}



        