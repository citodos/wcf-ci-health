﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfCiHealth
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SLocaisExec" in both code and config file together.
    public class SLocaisExec : ISLocaisExec
    {
        CIHealthEntities context = new CIHealthEntities();
       
        //CadastroLocais
        public Int32 GetCadastroLocais(Int32 idorgunidade,
                                        Int32 idconvenio,
                                        DateTime dtcadastro,
                                        Int32 idendereco, 
                                        String dslocal = "",
                                        String cdoperadora = "",
                                        String cnes = "", 
                                        String tipolocal = "", 
                                        String flg_status = "")
                                        
        {
            LocalExecSolic Loc = new LocalExecSolic();
            EnderecoMov mov = new EnderecoMov();
            try
            {
                Loc.idorgunidade = idorgunidade;
                //Loc.idconvenio = idconvenio;
                //Loc.dtcadastro = dtcadastro;
                Loc.dslocal = dslocal;
                Loc.cdoperadora = cdoperadora;
                Loc.cnes = cnes;
                Loc.tipolocal = tipolocal;
                Loc.flg_status = flg_status;

                context.LocalExecSolics.Add(Loc);
                
                Int32 idlocal = Loc.idlocal;
                mov.idendereco = idendereco;
                mov.idlocal = idlocal;
                mov.idorgunidade = idorgunidade;
                context.EnderecoMovs.Add(mov);  
                context.SaveChanges();

                return Loc.idlocal;

            }
            catch (Exception e)
            {
                return 0;
            }

        }

        //Pesquisa Locais
        public List<LocalExecSolic> GetPesquisaLocais(Int32 idlocal) 
                                                                    
        {
            if (idlocal != 0 && idlocal != null)
            {
                List<LocalExecSolic> LocaisExec = new List<LocalExecSolic>();
                LocalExecSolic Loc = new LocalExecSolic();

                foreach (var result in context.LocalExecSolics.Where(d => d.idlocal == idlocal))
                {
                    Loc = new LocalExecSolic();
                    Loc.idlocal = result.idlocal;
                    Loc.idorgunidade = result.idorgunidade;
                    Loc.idconvenio = result.idconvenio;
                    Loc.dtcadastro = result.dtcadastro;
                    Loc.dslocal = result.dslocal;
                    Loc.cdoperadora = result.cdoperadora;
                    Loc.cnes = result.cnes;
                    Loc.tipolocal = result.tipolocal;
                    Loc.flg_status = result.flg_status;
                    LocaisExec.Add(Loc);
                }

                return LocaisExec;
            }

            else
            {
                return null;

            }
        }

        //Excluir Locais
        public String GetExcluirLocais(Int32 idLocal)
        {
          try
            {
                var Deletarunidade = context.LocalExecSolics.FirstOrDefault(p => p.idlocal == idLocal);
                var Deletarmov = context.EnderecoMovs.FirstOrDefault(a => a.idlocal == idLocal);
                context.LocalExecSolics.Remove(Deletarunidade);
                context.EnderecoMovs.Remove(Deletarmov);
                context.SaveChanges();

                return "Exclusão realizada com Sucesso";
            }
            catch (Exception e)
            {
                return "Não Foi Possivel Realizar a Exclusão";
            }

        }

        //Alterar Locais
        public String GetAlterarLocais(Int32 idLocal,
                                        Int32 idorgunidade,
                                        Int32 idconvenio,
                                        DateTime dtcadastro,
                                        String dslocal = "",
                                        String cdoperadora = "",
                                        String cnes = "",
                                        String tipolocal = "",
                                        String flg_status = "")
                                        
        {
         try
              {
                  var atualizaLocal = context.LocalExecSolics.FirstOrDefault(p => p.idlocal == idLocal);
                  if (atualizaLocal != null)
                  {
                     
                      atualizaLocal.idorgunidade = idorgunidade;
                      atualizaLocal.idconvenio = idconvenio;
                      atualizaLocal.dtcadastro = dtcadastro;
                      atualizaLocal.dslocal = dslocal;
                      atualizaLocal.cdoperadora = cdoperadora;
                      atualizaLocal.cnes = cnes;
                      atualizaLocal.tipolocal = tipolocal;
                      atualizaLocal.idorgunidade = idorgunidade;
                      atualizaLocal.flg_status = flg_status; 
                      context.SaveChanges();

                      return "Alteração realizada com Sucesso";
                  }
                  else
                  {
                      return "Este Local não consta em nossos registros";
                  }

              }
              catch (Exception e)
              {
                  return "Não Foi Possivel Realizar a Alteração";
              }
          

        }

        public int idlocal { get; set; }
    }
}
