﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfCiHealth
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SContatos" in both code and config file together.
    public class SContatos : ISContatos
    {
        CIHealthEntities context = new CIHealthEntities();

        //CadastroContato
        public Int32 GetCadastroContato(Int32 idcontatotipo,
                                              Int32 idorgunidade,
                                              String dscontato = "",
                                              String nomecontato = ""
                                          )
        {
            Contato con = new Contato();
            try
            {
                con.idcontatotipo = idcontatotipo;
                con.dscontato = dscontato;
                con.nomecontato = nomecontato;

                context.Contatoes.Add(con);
                context.SaveChanges();

                Int32 IdContato = con.idcontato;

                ContatoMov mov = new ContatoMov();
                mov.idcontato = IdContato;
                mov.idorgunidade = idorgunidade;
                context.ContatoMovs.Add(mov);
                context.SaveChanges();
                    
                return IdContato;

            }
            catch (Exception e)
            {
                return 0;
            }

        }

        //PesquisaContato
        public List<Contato> GetPesquisaContato(Int32 idcontato
            )
        {
            List<Contato> contato = new List<Contato>();
            if (idcontato != 0 && idcontato != null)
            {
               
                Contato con = new Contato();

                foreach (var result in context.Contatoes.Where(d => d.idcontato == idcontato))
                {
                    con = new Contato();
                    con.idcontato = result.idcontato;
                    con.idcontatotipo = result.idcontatotipo;
                    con.dscontato = result.dscontato;
                    con.nomecontato = result.nomecontato;
                    contato.Add(con);
                }

              
            }
            return contato;
        }

        //DeletarContato
        public String GetExcluirContato(Int32 idcontato
            )
        {
            try
            {
                var Deletarcontato = context.Contatoes.FirstOrDefault(p => p.idcontato == idcontato);
                context.Contatoes.Remove(Deletarcontato);
                context.SaveChanges();

                return "Exclusão realizada com Sucesso";
            }
            catch (Exception e)
            {
                return "Não Foi Possivel Realizar a Exclusão";
            }
        }

        //AlterarContato
        public String GetAlterarContato(Int32 idcontato,
                                               Int32 idorgunidade,
                                               Int32 idcontatotipo,
                                               String dscontato = "",
                                               String nomecontato = ""
            )
        {
            try
            {
                var atualizacontato = context.Contatoes.FirstOrDefault(p => p.idcontato == idcontato);
                if (atualizacontato != null)
                {

                    atualizacontato.idcontato = idcontato;
                    atualizacontato.idcontatotipo = idcontatotipo;
                    atualizacontato.dscontato = dscontato;
                    atualizacontato.nomecontato = nomecontato;
                    context.SaveChanges();
                    return "Alteração realizada com Sucesso";
                }
                else
                {
                    return "Este contato não consta em nossos registros";
                }

            }
            catch (Exception e)
            {
                return "Não Foi Possivel Realizar a Alteração";
            }

        }
    }
}
