//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WcfCiHealth
{
    using System;
    using System.Collections.Generic;
    
    public partial class ContatoMov
    {
        public int idcontato { get; set; }
        public Nullable<int> idconvenio { get; set; }
        public Nullable<int> idprofequip { get; set; }
        public Nullable<int> idpaciente { get; set; }
        public Nullable<int> idorgunidade { get; set; }
    
        public virtual Contato Contato { get; set; }
        public virtual Empresa Empresa { get; set; }
        public virtual Paciente Paciente { get; set; }
        public virtual Profissional Profissional { get; set; }
        public virtual Convenio Convenio { get; set; }
    }
}
